package jonathan.moviles.epn.sharepreferences

import android.content.Context
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {

    internal lateinit var colorPantalla: EditText;
    internal lateinit var savedColor: Button;
    internal lateinit var fondoPantalla: RelativeLayout;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        colorPantalla = findViewById<EditText>(R.id.txt_ingresoColor);
        savedColor = findViewById<Button>(R.id.btn_save_color);
        fondoPantalla = findViewById<RelativeLayout>(R.id.fondo)
        cargarColor();
        savedColor.setOnClickListener{ _ -> guardarColor()};
    }

    private fun guardarColor (){
        val sharedPreferences = getSharedPreferences("colorInfo", Context.MODE_PRIVATE);
        val colorIngresado = this.colorPantalla.getText().toString();
        val editor = sharedPreferences.edit();
        editor.putString("color", colorIngresado);
        editor.commit();
        this.colorPantalla.setText("");
        fondoPantalla.setBackgroundColor(Color.parseColor("#FFFFFF"))
        Toast.makeText(this, sharedPreferences.getString
            ("color",""), Toast.LENGTH_LONG).show()
        cargarColor();
    }

    private fun cargarColor(){
        val sharedPreferences = getSharedPreferences("colorInfo", Context.MODE_PRIVATE);
        when(sharedPreferences.getString ("color","")){
            "azul" -> fondoPantalla.setBackgroundColor(Color.BLUE)
            "amarillo" -> fondoPantalla.setBackgroundColor(Color.YELLOW)
            "verde" -> fondoPantalla.setBackgroundColor(Color.GREEN)
            "cyan" -> fondoPantalla.setBackgroundColor(Color.CYAN)
            else -> fondoPantalla.setBackgroundColor(Color.WHITE)
        }
    }
}
